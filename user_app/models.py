from django.db import models
# from djangotoolbox.fields import EmbeddedModelField, ListField

# Create your models here.


from django.db import models

# Creating Three Model For User Based Acitity 
# All three user Will be Attached with Each Other with one primary key

class ActivityPeriods(models.Model):
    start_time = models.CharField(max_length=20)
    end_time = models.CharField(max_length=20)
    user_id = models.CharField(max_length=20)

    def __str__(self):
        return self.start_time, self.end_time


class Member(models.Model):
    user_id = models.CharField(max_length=20)  
    real_name = models.CharField(default="", max_length=20)
    tz = models.CharField(default="", max_length=20)
    activity_periods = models.ManyToManyField(ActivityPeriods)
    
    def __str__(self):
        return self.user_id, self.real_name, self.tz, self.activity_periods


class User(models.Model):
    ok = models.BooleanField(default=False)
    members = models.ManyToManyField(Member)
    class Meta:
        ordering = ['ok']

    def __str__(self):
        return self.ok, self.members








# class Member(models.Model):
#     # id = models.CharField(default="",max_length=20) 
#     real_name = models.CharField(default="", max_length=20)
#     tz = models.CharField(default="", max_length=20)

#     # class Meta:
#     #     ordering = ['title']

#     def __str__(self):
#         return self.real_name, self.tz

# class NewUser(models.Model):
#     ok = models.BooleanField(default=True)
#     members = models.ManyToManyField(Member)
#     # ListField(EmbeddedModelField('Member'))