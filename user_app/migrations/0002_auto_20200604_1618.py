# Generated by Django 2.2.13 on 2020-06-04 16:18

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('user_app', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='activityperiods',
            name='member_reference',
        ),
        migrations.RemoveField(
            model_name='member',
            name='user_reference',
        ),
        migrations.AddField(
            model_name='activityperiods',
            name='user_id',
            field=models.CharField(default=1, max_length=20),
            preserve_default=False,
        ),
    ]
