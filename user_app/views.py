from django.shortcuts import render
from .models import User, Member, ActivityPeriods
from django.views.decorators.csrf import csrf_exempt
import json
from django.core.exceptions import ObjectDoesNotExist
from django.http import JsonResponse
from django.http import HttpResponse

# Create your views here.

# Rendering on Home Page For Seeing User Activity
 
def home_page(request):
    return render(request,'index.html')


def check_user_register(user_id):
    try:
        data = Member.objects.get(user_id=user_id)    
    except ObjectDoesNotExist as exception:
        print("User not Exist")
        data =None
    return data   


# Function For Uploading data And Fetching Stored Data from Database

@csrf_exempt
def user_activity_(request):
    if request.method == 'POST':
        json_data = json.loads(request.body)
        # user_id = json_data[""]
        if "members" not in list(json_data.keys()):
            return render(request, 'response_result.html', {"name":"Please  Give Appropriate Data"})
            
        try:
            for load_data in json_data['members']:
                input_id = load_data['id']
                real_name = load_data['real_name']
                tz = load_data['tz']
                

                member = check_user_register(input_id)
                user = User()
                user.save()
            if member:
                for date_data in load_data["activity_periods"]:
                    start_date = date_data["start_time"]
                    end_date = date_data["end_time"]
                    activity = ActivityPeriods(start_time=start_date, end_time=end_date, user_id=input_id)
            
                    activity.save()
                    member.activity_periods.add(activity)
                    # member.user_reference = user._id
                    member.save()
                    user.members.add(member)
                user.save()

            else:
                if "activity_periods" not in list(load_data.keys()):
                    render(request, 'index.html',{"name":"Please  Give Appropriate Data"})
                   
                add_member = Member(user_id=input_id, real_name=real_name, tz=tz)
                add_member.save()
                for date_data in load_data["activity_periods"]:
                    start_date = date_data["start_time"]
                    end_date = date_data["end_time"]
                    activity = ActivityPeriods(start_time=start_date, end_time=end_date, user_id=input_id)
                    activity.save()
                    add_member.activity_periods.add(activity)
                add_member.save()
                user.members.add(add_member)
                user.save()

        except Exception as e:
            pass        
        return  render(request, 'response_result.html',{"name":"SUccessss Fetched Data"})  
   
    elif request.method == "GET":
        try:
            fetch_all_member = Member.objects.all()
        # fetch_all_activity = ActivityPeriods.objects.all()    
            display_result = list()
            # members_data = list()
            activity_periods = list()
            member_dict = {} 
            final_json_ = {"ok":True}
            for fetch_data in fetch_all_member:
                temp_data = ActivityPeriods.objects.filter(user_id=fetch_data.user_id)

                for data in temp_data:
                    activity_periods.append({"start_time":data.start_time,"end_time":data.end_time})
                # display_json_["members"] ={}
                member_dict["id"] = fetch_data.user_id
                member_dict["real_name"] =fetch_data.real_name
                member_dict["tz"] =  fetch_data.tz
                member_dict["activity_periods"] = activity_periods
                display_result.append(member_dict)
            final_json_["members"] = display_result   
            dump = json.dumps(final_json_, indent=4 )
            response = HttpResponse(dump, content_type='application/json')
            response['Content-Disposition'] = 'attachment; filename="Test_json_data.json"'
                
        except Exception as e:
            pass
        return response
















        
        # HttpResponse(dump, content_type='application/json')
        # return JsonResponse(final_json_)
     
        # return render(request, 'index.html',{"name":final_json_})
    # p1.save()
    

    # a1 = Article(headline='Second Heading')
    
    # a1.publications.add(p1)
    # temp = list()
    # for data in Article.objects.all():

    #     temp.append({"headline":data.headline, "publication":data.publications.all()[0].title})

    # a2 = Article(headline='NASA uses Python')
    # a2.save()
    # a2.publications.add(p1, p2)
    # a2.publications.add(p3)

    # user = NewUser(ok= True)
    # user.save()
    # member = Member(real_name = "ashish", tz = "XYZ")

    # member.save()

    
    # # user.members.add(members=member)
    # user.members.add(real_name = "ashish", tz = "XYZ")

    # # member.real_name = "ashish"
    # # member.tz = "XYZ"
    # # user.members = member
    
    # user.save()
    # print(user,"here")
    

