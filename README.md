# User_Activity


Project Description :
                    Created Django Web Application basically used for Manage user activity
                    1) We Can Upload the data 
                    3) We Can Get User Activity data in JSON file

Tool And Language USed :
                    Python 3.6
                    Mongodb 3.x
                    Django FrameWork 2.x
                    Git for Version Control
                    Aws (EC2 instance) for deployment
                    Postman


Access Server End Point
    1) Landing on Home Page
    http://13.235.36.255:8000/
    2) Uploading Json Data (have Followed As Sample data)
    http://13.235.36.255:8000/load_data                    
    3) for Downloading Json File you can click on hyperlink you will get on home page
    Ex: Click Here For Download User Activity Json File $


For uploadin Data i have used POSTMAN
This is Collection of Uploded USer Data
https://www.postman.com/collections/ec199c4db7ce6fa111ae